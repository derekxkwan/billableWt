uspsDom = 194.0
upsFedDom = 166.0

uspsIntl = 166.0
upsFedItl = 139.0

try: input = raw_input
except NameError: pass

domIntl = input("Domestic or International? Enter dom or intl: ")
carrier = input("USPS, UPS or FedEx?: ")
dimStr= input("Enter dimensions in inches separated by commas (L,W,H):  ")
dimArr = dimStr.split(",")
while len(dimArr) != 3:
    input("Incorrect number of dimensions. Try again: ")
dimArr = map(lambda x: float(x), dimArr)
dim = dimArr[0] * dimArr[1] * dimArr[2]
wt = input("Enter weight in lbs: ")



if (domIntl.lower() == "dom" and (carrier.lower() == "ups" or carrier.lower() == "fedex")) or (domIntl.lower() == "intl" and (carrier.lower() == "usps")):
    dimWt = dim/166.0
elif domIntl.lower() == "dom" and carrier.lower() == "ups":
    dimWt = dim/139.0
else:
    dimWt = dim/194.0

if float(wt) >= dimWt:
    billWt = float(wt)
else:
    billWt = dimWt

print("Billable Weight: %f lbs" %(billWt))
